/*
    Javascript ActivityPub Software
    Copyright (C) 2018 Gregory Sartucci
    License: AGPL-3.0, Check file LICENSE
*/

Package.describe({
    name: 'agoraforum:activitypub',
    version: '0.0.3',
    summary: 'ActivityPub Library',
    git: 'https://github.com/Agora-Project/JSActivityPub',
    documentation: 'README.md'
});

Package.onUse(function(api) {
    both = ['client', 'server'];

    api.addFiles([
        'lib/types.js'
    ], both);

    api.use([
        'ecmascript@0.12.4'
    ], both);

    api.export([
        'activityPubActorTypes',
        'activityPubContentTypes',
        'activityPubActivityTypes',
        'activityPubSchemas',
        'ActivityPubObject',
        'ActivityPubActivity'
    ]);
});

Package.onTest(function(api) {
    api.use('tinytest');
    api.use('agoraforum:activitypub', ['client', 'server']);
});
